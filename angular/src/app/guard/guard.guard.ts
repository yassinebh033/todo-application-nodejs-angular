import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from "@auth0/angular-jwt";

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  constructor(private _router : Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      let token = localStorage.getItem('token');
      if (token){
          const helper = new JwtHelperService();
          const decodedToken = helper.decodeToken(token);
      let role =decodedToken.role;
        if(role=="user"){
          return true;
        }/*else{
          //fama token mais role nest pas user
          this._router.navigate(['/config']);
          return false;
        }*/

      }else{
        //mafamch token dc mch connecter
            this._router.navigate(['/registre']);
            return false;
          }
  }
}
