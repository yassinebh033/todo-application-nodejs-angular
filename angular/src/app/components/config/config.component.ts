import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit {
  users:any;
  constructor(private connect : ConnexionService, private toastr: ToastrService) { }

  ngOnInit() {
    this.getAllUser();
  }

  getAllUser(){
    this.connect.getAllUsers()
    .subscribe(
      (res) => {
        console.log(res);
        this.users=res;
      },
      (err) => {

      console.log(err);
      
      }
    );
}
changerEtat(value){
  console.log(value);
  this.connect.configurer(value)
  .subscribe(
    (res) => {
      console.log(res);
      this.toastr.success(res.message);
      window.location.reload();
    },
    (err) => {
      this.toastr.error(err);
    console.log(err);
    
    }
  );
}
}
