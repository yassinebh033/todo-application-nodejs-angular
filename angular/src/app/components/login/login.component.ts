import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from "@angular/forms";
import { ConnexionService } from '../../services/connexion.service'
import { User } from 'src/app/classes/user';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  decodedToken :any;
  loginForm: FormGroup;
  constructor(private _formBuilder: FormBuilder, private auth: ConnexionService, private _router: Router, private toastr: ToastrService) {

    this.loginForm = this._formBuilder.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(8)])
    })
  }
  get email() { return this.loginForm.get('email') };
  get password() { return this.loginForm.get('password') };

  ngOnInit() {

    
  }

  login() {
    console.log(this.loginForm.value);
    let data = this.loginForm.value;
    let user = new User(null, null, null,null,data.email, data.password);
    this.auth.login(user)
      .subscribe(
        (res) => {
          console.log(res);
          let token = res.token;
          localStorage.setItem('token', token);
          const helper = new JwtHelperService();
          let decodedToken = helper.decodeToken(token);
          console.log(decodedToken);
          localStorage.setItem('id',decodedToken.id);
          localStorage.setItem('role',decodedToken.role);
         let role =decodedToken.role;
        if(role=="user"){
          this.toastr.success("welcome");
          setTimeout(() => {
             this._router.navigate(['/check']);
          }, 500);
         
}
else{
  this.toastr.success("welcome admin");
          this._router.navigate(['/config']);
}
        },

        (err) => {
          this.toastr.error(err.error.message);
        console.log(err);
        
        }
      );
  }

}



