import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/classes/user';
import { ConnexionService } from 'src/app/services/connexion.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent implements OnInit {
  registerForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private auth: ConnexionService, private _router: Router, private toastr: ToastrService) {
    this.registerForm = this.formBuilder.group({
      firstname: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-zA-Z]+')
      ]),
      lastname: new FormControl('', [
        Validators.required,
        Validators.pattern('[a-zA-Z]+')
      ]),
      phone: new FormControl('', [
        Validators.required,
        Validators.pattern('[0-9]*'),
        Validators.minLength(8),
        Validators.maxLength(13)]),
      email: new FormControl('', [
        Validators.required,
        Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(8)])
      // ,acceptTerms: [false, Validators.requiredTrue]
    });
  }

  ngOnInit() { }


  get email() { return this.registerForm.get('email') }
  get password() { return this.registerForm.get('password') }
  get lastname() { return this.registerForm.get('lastname') }
  get firstname() { return this.registerForm.get('firstname') }
  get phone() { return this.registerForm.get('phone') }


  register() {

    console.log(this.registerForm.value)
    let data = this.registerForm.value;
    const user = new User(null,data.firstname,data.lastname,data.phone,data.email,data.password);
    console.log(user);

    this.auth.register(user)
      .subscribe(
        (res) => {
          console.log(res.message);

          this.toastr.success(res.message);

          this._router.navigate(['/login']);
        },

        (err) => {
          this.toastr.error("email exist déja");
        }
      );
  }

  onReset() {
    this.registerForm.reset();
  }

  showSuccess() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

}
