import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  userConnect:any;
  idConnected:any;
  constructor(private connect:ConnexionService) { }

  ngOnInit() {
    this.idConnected=localStorage.getItem('id');
    this.getConnected();
  }
  getConnected(){  
    
    this.connect.getOneUser(this.idConnected)
    .subscribe(
      (res) => {
        this.userConnect=res;

      },
      (err) => {
      console.log(err);
      }
    );
}

}
