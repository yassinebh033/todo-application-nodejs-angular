import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  connectedUserID:any;
  connectedUserRole:any;
  role:any;
  constructor(private connect: ConnexionService) {  }

  ngOnInit() {
  this.role=this.connect.role;
console.log(this.role);
  }

  
  loggout(){
    return this.connect.logOut();
    
  }
  logged(){
    return this.connect.loggedIn();
  }

}
