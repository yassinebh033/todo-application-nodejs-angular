import { Component, OnInit } from '@angular/core';
import { ConnexionService } from 'src/app/services/connexion.service';
import { ListesService } from 'src/app/services/listes.service';
import { User } from 'src/app/classes/user';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Todo } from 'src/app/classes/todo';

@Component({
  selector: 'app-check-list',
  templateUrl: './check-list.component.html',
  styleUrls: ['./check-list.component.css']
})
export class CheckListComponent implements OnInit {
  idUserConnected: any;
  tabtodo: any = [];
  tabdoing:any=[];
  tabdone: any = [];
  addlist: FormGroup;
  modifGroup: FormGroup;
affichageinput:Boolean=false;
id_todo:any;
  constructor(private todoService: ListesService, private toastr: ToastrService) {
    this.addlist = new FormGroup({
      description: new FormControl("")
    })
    this.modifGroup=new FormGroup({
      modify: new FormControl("")
    })
  }

  ngOnInit() {
    this.idUserConnected = localStorage.getItem('id');
    this.getAllTodo(this.idUserConnected);
  }


  //get text description pour ajouter todo
  get_description() {
    return this.addlist.get('description').value;
  }
  //get text description aprés modification
  get_modify(){
  return this.modifGroup.get('modify').value;
  }
//changer la description to do par un input de text 
  modif(todo){
    if (this.affichageinput==false){
       this.id_todo=todo._id;
      return this.affichageinput=true;
    }else{
      this.id_todo=todo._id;
      return this.affichageinput=false;
    }
  }
//get all to do
  getAllTodo(value) {
    console.log(value);
    this.todoService.getAllList(value)
      .subscribe(
        (res) => {
          console.log(res);
          this.tabtodo = res.todoList;
          this.tabdoing = res.doingList;
          this.tabdone = res.doneList;
        },
        (err) => {
          console.log(err);
        }
      );
  }
//ajouter todo
  ajouter() {
    let todoDescription = this.get_description();

    let todo = new Todo(null, todoDescription, null, null, null, this.idUserConnected);
    console.log(todo);
    this.todoService.addTodo(todo)
      .subscribe(
        (res) => {
          console.log(res);
          this.toastr.success(res.message);
          this.addlist.reset();
          this.idUserConnected = localStorage.getItem('id');
    this.getAllTodo(this.idUserConnected);
        },
        (err) => {
          console.log(err);
          this.toastr.success(err.error.message);
        }
      );
  }
//doing ==> done
doing(todo) {
  let index = this.tabtodo.indexOf(todo);
  this.tabtodo.splice(index,1);
  this.tabdoing.push(todo);
 
  this.todoService.doingtodo(todo._id)
    .subscribe(
      (res) => {
        console.log(res);
        this.toastr.success(res.message);

      },
      (err) => {
        console.log(err);
      }
    );
}

//doing ==> done
  done(doing) {
    let index = this.tabtodo.indexOf(doing);
    this.tabdoing.splice(index,1);
    doing.dateFin = new Date();
    this.tabdone.push(doing);
   
    this.todoService.doneTodo(doing._id)
      .subscribe(
        (res) => {
          console.log(res);
          this.toastr.success(res.message);

        },
        (err) => {
          console.log(err);
        }
      );
  }

  //modifier texte todo
  modifier(todo) {
let Newtodo=new Todo(todo._id,this.get_modify(),null,null,null,null);
    this.todoService.updateTodo(Newtodo)
      .subscribe(
        (res) => {
          console.log(res);
          let index = this.tabtodo.indexOf(todo);
          this.tabtodo.splice(index,1);
          todo.description=this.get_modify();
          this.tabtodo.push(todo);
          this.toastr.success(res.message);
          this.affichageinput=false;
          console.log(todo);

        },
        (err) => {
          console.log(err);
        }
      );
  }

  //delete todo
  supprimertodo(todo) {
    
    let index = this.tabtodo.indexOf(todo);
    this.tabtodo.splice(index,1);

    
    
    this.todoService.deleteTodo(todo._id)
      .subscribe(
        (res) => {
          console.log(res);
          this.toastr.success(res.message);
        },
        (err) => {
          console.log(err);
        }
      );

  }

  supprimerdoing(doing) {
    
    let index = this.tabdoing.indexOf(doing);
    this.tabdoing.splice(index,1);
  
    
    
    this.todoService.deleteTodo(doing._id)
      .subscribe(
        (res) => {
          console.log(res);
          this.toastr.success(res.message);
        },
        (err) => {
          console.log(err);
        }
      );
  
  }


 //delete todo
 supprimerdone(done) {
    
  let index = this.tabdone.indexOf(done);
  this.tabdone.splice(index,1);

  
  
  this.todoService.deleteTodo(done._id)
    .subscribe(
      (res) => {
        console.log(res);
        this.toastr.success(res.message);
      },
      (err) => {
        console.log(err);
      }
    );

}

}
