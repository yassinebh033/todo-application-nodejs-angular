import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistreComponent } from './components/registre/registre.component';
import { LoginComponent } from './components/login/login.component';
import { CheckListComponent } from './components/check-list/check-list.component';
import { GuardGuard } from './guard/guard.guard';
import { ConfigComponent } from './components/config/config.component';
import { AdminGuard } from './guard/admin.guard';
import { ProfilComponent } from './components/profil/profil.component';



const routes: Routes = [
  {'path':"registre", component:RegistreComponent},
  {'path':"login", component:LoginComponent},
  {'path':"check", component:CheckListComponent,canActivate:[GuardGuard]},
  {'path':"config", component:ConfigComponent,canActivate:[AdminGuard]},
  {'path':"profil", component:ProfilComponent},

  {'path':"**", component:LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
