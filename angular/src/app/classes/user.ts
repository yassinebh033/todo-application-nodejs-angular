export class User {
   constructor( 
     private _id?: String,
    private _firstname?: String,
    private _lastname?: String,
    private _phone?: String,
    private _email?: String,
    private _password?: String,
   ){}

get_id(){
     return this._id;
   }
set_id(value){
  return this._id =value;
}
get_firstname(){
  return this._firstname;
}
set_firstname(value){
return this._firstname =value;
}
get_lastname(){
  return this._lastname;
}
set_lastname(value){
return this._lastname =value;
}
get_phone(){
  return this._phone;
}
set_phone(value){
return this._phone =value;
}
get_email(){
  return this._email;
}
set_email(value){
return this._email =value;
}
get_password(){
  return this._password;
}
set_password(value){
return this._password =value;
}

}

