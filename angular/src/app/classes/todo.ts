export class Todo {
    constructor(
        private _id : String,
        private _description : String,
        private _dateAjout : Date,
        private _dateFin : Date,
        private _etat : boolean,
        private _idUser : String
    ){}



    get_id(){
        return this._id;
    }
    set_id(value){
        this._id=value;
    }
    get_description(){
        return this._description;
    }
    set_description(value){
        this._description=value;
    }
    get_dateAjout(){
        return this._dateAjout;
    }
    set_dateAjout(value){
        this._dateAjout=value;
    }
    get_dateFin(){
        return this._dateFin;
    }
    set_dateFin(value){
        this._dateFin=value;
    }   
    get_etat(){
        return this._etat;
    }
    set_etat(value){
        this._etat=value;
    }

    get_idUser(){
        return this._idUser;
    }
    set_idUser(value){
        this._idUser=value;
    }


}
