import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders} from '@angular/common/http'
import { User } from '../classes/user';
import { Router } from '@angular/router';
const httpOptions={ headers : new HttpHeaders({'Content-Type' : 'application/json'})};
const httpOptions1={ headers :new HttpHeaders().append('token', localStorage.getItem('token')) };
@Injectable({
  providedIn: 'root'
})
export class ConnexionService {
  role :any;
constructor( private http : HttpClient, private _router: Router) { 
 this.role=localStorage.getItem('role');
}

register(user :User){
  let registreUrl="http://localhost:3000/user/registre";
return this.http.post<any>(registreUrl,user,httpOptions);
}
login(user :User){
  let loginUrl="http://localhost:3000/user/login";
  return this.http.post<any>(loginUrl,user,httpOptions);
}
configurer(id : String){
  let configUrl="http://localhost:3000/user/config/"+id;
  return this.http.put<any>(configUrl,id,httpOptions1);
}
getAllUsers(){
let listUsersUrl="http://localhost:3000/user/listUsers";
return this.http.get(listUsersUrl,httpOptions1);
}
getOneUser(id:any){
  let UserUrl="http://localhost:3000/user/getUser/"+id;
  return this.http.get(UserUrl,httpOptions1);
}

getToken(){
  return localStorage.getItem('token');
  }
  
loggedIn(){
return !! localStorage.getItem('token')
}
  
logOut(){
  localStorage.clear();
  this._router.navigate(['../sign']);
  }
  

}
