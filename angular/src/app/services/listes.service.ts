import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Todo } from '../classes/todo';
const httpOptions1 = { headers: new HttpHeaders().append('token', localStorage.getItem('token')) };

@Injectable({
  providedIn: 'root'
})
export class ListesService {

  constructor(private http: HttpClient) { }

  getAllList(id_user: String) {
    let listUrl = "http://localhost:3000/todolist/list/" + id_user;
    return this.http.get<any>(listUrl, httpOptions1);
  }


  addTodo(todo: any) {
    let todoUrl = "http://localhost:3000/todolist/add/";
    return this.http.post<any>(todoUrl, todo, httpOptions1);
  }

  deleteTodo(id_todo: any) {
    let todoUrl = "http://localhost:3000/todolist/deletelist/" + id_todo;
    return this.http.delete<any>(todoUrl, httpOptions1);
  }

  updateTodo(todo: Todo) {
    let todoUrl = "http://localhost:3000/todolist/update";
    return this.http.put<any>(todoUrl, todo, httpOptions1);
  }

  doneTodo(idtodo : any) {
    let todoDone= new Todo(idtodo,null,null,null,null,null);
    let todoUrl = "http://localhost:3000/todolist/done";
    return this.http.put<any>(todoUrl, todoDone, httpOptions1);
  }
  doingtodo(idtodo : any) {
    let todoDoing= new Todo(idtodo,null,null,null,null,null);
    let todoUrl = "http://localhost:3000/todolist/doing";
    return this.http.put<any>(todoUrl, todoDoing, httpOptions1);
  }
}
