import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { DropdownModule, CollapseModule, CheckboxModule, WavesModule,ButtonsModule, InputsModule, IconsModule, CardsModule  } from 'angular-bootstrap-md';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegistreComponent } from './components/registre/registre.component';
import { CheckListComponent } from './components/check-list/check-list.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {
  ToastrModule,
  ToastNoAnimation,
  ToastNoAnimationModule
} from 'ngx-toastr';
import { ConfigComponent } from './components/config/config.component';
import { ProfilComponent } from './components/profil/profil.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistreComponent,
    CheckListComponent,
    NavbarComponent,
    ConfigComponent,
    ProfilComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule ,
    BrowserAnimationsModule,
    CollapseModule,
    DropdownModule.forRoot() ,
    CheckboxModule, 
    WavesModule.forRoot(), 
    ButtonsModule, 
    InputsModule, 
    IconsModule, 
    CardsModule,
    CardsModule.forRoot(),
    ToastNoAnimationModule.forRoot(),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
