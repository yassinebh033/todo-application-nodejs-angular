const mongoose= require("mongoose");


const todo_schema= new mongoose.Schema({
    description: {
        type:String,
        required:true
    },
    dateAjout: {
        type:Date,
        default: new Date(),
    },
    dateFin: {
        type:Date,
        default: null,

    },
    etat: {
        type: String,
        default: "todo"
    },
    idUser: {
        type: String,
        required: true
    }
});
const Todo= mongoose.model("todo",todo_schema);

module.exports={Todo};