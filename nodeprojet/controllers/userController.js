const express= require("express");
const app = express(); //à travers ça je peux faire la creation des services
const {mongoose}= require("./../db/config");
const {User}= require ("./../models/user");
const jwt = require ("jsonwebtoken");

//service registre
app.post("/registre", (req,res)=>{
    let data = req.body;

    let user= new User({
        firstname: data._firstname,
        lastname: data._lastname,
        phone: data._phone,
        email: data._email,
        password: data._password
    })
    user.save().then((userFromDb)=>{
        console.log(userFromDb);
        res.status(200).send({message:"registration done"});
    }).catch((error)=>{
        res.status(400).send(error);
    })
});

//service login
app.post("/login", (req,res)=>{
    let data=req.body;
    console.log(data);
User.findOne({email:data._email, password: data._password}).then((userFromDb)=>{
    
    if (!userFromDb){
        res.status(404).send({message:"Email/pswd incorrect"});
    }
else{
    let token=jwt.sign({id: userFromDb._id, role:userFromDb.role},"mykey")
    //on est entrain d'envoyer le token dans response donc dans le headers du coup dans le body on reçoit le msg logged
    //res.set('token',token);
    //res.status(200).send({message:"user logged"});
   //on envoie dans le body le token
    res.status(200).send({token});
}
}).catch((error)=>{
    res.status(404).send("erreur :"+error);
})
});

app.get("/getUser/:id", (req,res)=>{
    let id=req.params.id;
    console.log(id);
User.findOne({_id:id}).then((userFromDb)=>{
    res.status(200).send({userFromDb});
}).catch((error)=>{
    res.status(404).send("erreur :"+error);
})
});

//activer et desactiver les comptes
//_id cest l'attribut stocké dans la bd mangodb on le met tjrs _id
app.put("/config/:id", (req,res)=>{
    // on recupere le token à partir du headers
    let token= req.headers.token;
    try{
        let decryptedToken=jwt.verify(token,"mykey");
        let role =decryptedToken.role;
        if (role !="admin"){
            res.status(500).send({message:"you are not authorized to access here"})
        } else{
            let id= req.params.id;
            User.findOne({_id:id}).then((userFromDb)=>{
                userFromDb.etat= !userFromDb.etat;
                userFromDb.save();
                res.status(200).send({message:"Etat changed"});
            
            }).catch((error)=>{
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({message:"you are not authorized to access here"});
    }
});

//get all users
app.get("/listUsers",(req,res)=>{
    User.find({role:"user"}).then((users)=> {
        res.status(200).send(users);
    }).catch((error)=>{
        res.status(400).send(error);
    })
});


module.exports = app;
