const express = require("express");
const app = express(); //je déclare app de express biblio et à travers ça je peux faire des apis/services
const { mongoose } = require("./../db/config");
//import du model todo
const { Todo } = require("./../models/todo");
//import du jwt à l'aide de la biblio jsonwebtoken pour l'authentification
const jwt = require("jsonwebtoken");

//api get list avec id
app.get("/list/:id", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "you are not authorized to access here" })
        } else {
            let id = req.params.id;
            console.log(id);
            Todo.find({ idUser: id }).then((todos) => {

                let todoList = [];
                let doingList=[];
                let doneList = [];
                for (i = 0; i < todos.length; i++) {
                    if (todos[i].etat == "todo") {
                        todoList.push(todos[i])
                    } else if(todos[i].etat == "doing"){
                        doingList.push(todos[i])
                    }
                    else {
                        doneList.push(todos[i]);
                    }
                }
                res.status(200).send({ todoList,doingList, doneList })
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "you are not authorized to access here" });
    }
});

//api ajout todo
app.post("/add", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "you are not authorized to access here" })
        } else {
            let data = req.body;
            console.log(data);
            let todo = new Todo({
                description: data._description,
                idUser: data._idUser
            })
            todo.save().then((todoFromDb) => {
                console.log(todoFromDb);
                res.status(200).send({ message: "todo added" });
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "you are not authorized to access here" });
    }
});


app.delete("/deletelist/:id", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "you are not authorized to access here" })
        } else {
            let idtodo = req.params.id;
            Todo.findOneAndDelete({ _id: idtodo }).then((todoFromDb) => {
                console.log(todoFromDb);
                res.status(200).send({ message: "todo deleted" });
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "you are not authorized to access here" });
    }
});



app.put("/update", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "you are not authorized to access here" })
        } else {
            let descript = req.body._description;
            let idtodo = req.body._id;
            Todo.findOneAndUpdate({ _id: idtodo }, { description: descript }).then((todoFromDb) => {
                console.log(todoFromDb);
                res.status(200).send({ message: "todo updated" });
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "you are not authorized to access here" });
    }
});

app.put("/doing", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "1you are not authorized to access here" })
        } else {
            let idtodo = req.body._id;
            console.log(idtodo);
            Todo.findOneAndUpdate({ _id: idtodo }, { etat: "doing" }).then((todoFromDb) => {
                console.log(todoFromDb);
                res.status(200).send({ message: "todo vers doing" });
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "2you are not authorized to access here" });
    }
});

app.put("/done", (req, res) => {
    let token = req.headers.token;
    try {
        let decryptedToken = jwt.verify(token, "mykey");
        let role = decryptedToken.role;
        if (role != "user") {
            res.status(500).send({ message: "1you are not authorized to access here" })
        } else {
            let idoing = req.body._id;
            console.log(idoing);
            Todo.findOneAndUpdate({ _id: idoing }, { etat: "done", dateFin: new Date() }).then((todoFromDb) => {
                console.log(todoFromDb);
                res.status(200).send({ message: "doing vers done" });
            }).catch((error) => {
                res.status(400).send(error);
            })
        }
    }
    catch{
        //ici la methode verify a détecté que token n'est pas un token donc try catch return erreur
        res.status(500).send({ message: "2you are not authorized to access here" });
    }
});
module.exports = app;
