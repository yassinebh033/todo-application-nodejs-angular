const mongoose = require ("mongoose");
//configuration de notre BD avec l'url lié
mongoose.connect("mongodb://localhost:27017/todoApp", {
useCreateIndex:true,
useNewUrlParser:true,
useUnifiedTopology:true,
useFindAndModify: false
});
