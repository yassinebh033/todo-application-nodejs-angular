// import 3 librairies
const express= require("express");
const bodyParser = require("body-parser");                   
const cors = require("cors");

const app = express(); //à travers ça je peux faire la création de service

app.use(bodyParser.json());
app.use(cors());


//lors de chargement et lancement du serveur
app.get("/",(req,res)=>res.status(200).send("welcome to serveur"));
//il va attendre le lancement du serveur et lire à partir du port 3000 et si il est strated affiche moi le serveur il est up.
app.listen(3000,  ()=>console.log("serveur started"));


/////////////////////////////////////////////////////////////////////////////////////////////////////
//import controller user
const userController= require("./controllers/userController");
//declaration du usercontroller donc pour tte requete  te vient sur ce path /user
//envoie là vers userController.js
app.use("/user",userController);


//import controller todolist
const todoController= require("./controllers/todoController");
//declaration de utilisation du todolist donc pour tte requete te vient sur ce path /user
//envoie là vers todoController.js
app.use("/todolist", todoController);



